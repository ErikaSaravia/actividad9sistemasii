/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pima_
 */
public class OperacionesTest {
    
    Operaciones frase1 = new Operaciones();
    
    @Test
    public void testContarPalabras0(){
        assertEquals(2, frase1.contarPalabras("Hola mundo"));
    }
    
    @Test
    public void testContarPalabras1(){
        assertEquals(3, frase1.contarPalabras("Contador de palabras"));
    }
    
    @Test
    public void testContarPalabras2(){
        assertEquals(6, frase1.contarPalabras("Me dormi ayer en la noche"));
    }
    
    Operaciones cliente = new Operaciones();
    
    @Test
    public void testTotal0(){
        assertEquals(12, cliente.TazasCompradas(11));
    }
    
    @Test
    public void testTotal1(){
        assertEquals(2, cliente.TazasCompradas(2));
    }
    
    @Test
    public void testTotal2(){
        assertEquals(21, cliente.TazasCompradas(18));
    }
    
    Operaciones numero = new Operaciones();
    
    @Test
    public void VerdaderoFalso0(){
        assertFalse(numero.VerdaderoFalso(5));
        //assertEquals(false, numero.VerdaderoFalso(5));
    }
    
    @Test
    public void VerdaderoFalso1(){
        assertTrue(numero.VerdaderoFalso(0));
        //assertEquals(true, numero.VerdaderoFalso(-2));
    }
    
    @Test
    public void VerdaderoFalso2(){
        //assertEquals(true, numero.VerdaderoFalso(0));
        assertFalse(numero.VerdaderoFalso(6));
    }
    
    
    Operaciones tartamudo1 = new Operaciones();
    
     @Test
    public void testTartamudo0(){
        assertEquals("Ho... Ho... Hola?", tartamudo1.Tartamudeo("Hola"));
    }
    
    @Test
    public void testTartamudo1(){
        assertEquals("Bu... Bu... Buenos dias?", tartamudo1.Tartamudeo("Buenos dias"));
    }
    
    @Test
    public void testTartamudo2(){
        assertEquals("Bi... Bi... Bienvenido?" ,tartamudo1.Tartamudeo("Bienvenido"));
    }

   
}
