/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pima_
 */
public class Operaciones {
    //1
     public int contarPalabras(String frase) {
        int contador = 1, pos;
        frase = frase.trim(); //eliminar los posibles espacios en blanco al principio y al final                              
        if (frase.isEmpty()) { //si la cadena está vacía
            contador = 0;
        } else {
            pos = frase.indexOf(" "); //se busca el primer espacio en blanco
            while (pos != -1) {   //mientras que se encuentre un espacio en blanco
                contador++;    //se cuenta una palabra
                pos = frase.indexOf(" ", pos + 1); //se busca el siguiente espacio en blanco                       
            }                                     //a continuación del actual
        }
        return contador;
    }
     //2
      public boolean VerdaderoFalso(int entrada) {
        boolean respuesta;
        respuesta = entrada <= 0;
        return respuesta;
    }
      //3
       public int TazasCompradas(int compradas) {
        int division = compradas/6;
        int total = compradas+division;
        return total;
    }
       //4
       public String Tartamudeo(String entrada) {
        String sSubCadena = entrada.substring(0, 2);
        String salida=(sSubCadena+"... "+sSubCadena+"... "+entrada+"?");
        return salida;
    }

}
